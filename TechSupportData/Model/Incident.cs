﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechSupportData.Model
{
    public class Incident
    {
        /// <summary>
        /// Set up basic getters and setters for variables that will be stored in an Incident
        /// </summary>

        public string IncidentID { get; set; }

        public string ProductCode { get; set; }

        public string Product { get; set; }

        public string DateOpened { get; set; }

        public string DateClosed { get; set; }

        public string Customer { get; set; }

        public string Technician { get; set; }

        public string TechID { get; set; }

        public string Title { get; set; }

        public string CustomerID { get; set; }

        public string Description { get; set; }
    }
}
