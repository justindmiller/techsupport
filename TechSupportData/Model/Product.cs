﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechSupportData.Model
{
    public class Product
    {
        ///Class to hold Product stored variables
        public string ProductName { get; set; }

        public string ProductCode { get; set; }

        public string Version { get; set; }

        public DateTime ReleaseDate { get; set; }
    }
}
