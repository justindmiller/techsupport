﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechSupportData.Model
{
    public class Customer
    {
        ///Class to hold Customer stored variables
        public string CustomerName { get; set; }

        public string CustomerID { get; set; }
    }
}
