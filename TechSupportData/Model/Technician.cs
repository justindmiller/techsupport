﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechSupportData.Model
{
    public class Technician
    {
        public string TechID { get; set; }

        public string Name { get; set;  }

        public string Email { get; set; }

        public string Phone { get; set; }
    }
}
