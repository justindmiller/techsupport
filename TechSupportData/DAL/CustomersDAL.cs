﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TechSupportData.Model;

namespace TechSupportData.DAL
{
    /// <summary>
    /// Retrives the customer information form the Database
    /// </summary>
    public class CustomersDAL
    {
        ///This method will retrive the customers from the database and populate them into Customer List
        public static List<Customer> GetCustomers()
        {
            List<Customer> customerList = new List<Customer>();
            SqlConnection connection = TechSupportDBConnection.GetConnection();
            string selectStatement =
                "SELECT CustomerID, Name " +
                "FROM Customers " +
                "ORDER BY Name";
            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            SqlDataReader reader = null;

            try
            {
                connection.Open();
                reader = selectCommand.ExecuteReader();

                while (reader.Read())
                {
                    Customer customer = new Customer();
                    customer.CustomerName = reader["Name"].ToString();
                    customer.CustomerID = reader["CustomerID"].ToString();
                    customerList.Add(customer);
                }

            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Something broke here! View");
                throw ex;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
                if (reader != null)
                    reader.Close();
            }


            return customerList;
        }

        public static Customer getCustomer(string request)
        {
            Customer customer = new Customer();
            SqlConnection connection = TechSupportDBConnection.GetConnection();
            string selectStatement = "SELECT Name " +
                                     "FROM Customers " +
                                     "WHERE @CustomerID = CustomerID";
            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            selectCommand.Parameters.AddWithValue("@CustomerID", request);
            SqlDataReader reader = null;

            try
            {
                connection.Open();
                reader = selectCommand.ExecuteReader();

                if (reader.Read())
                {
                    customer.CustomerID = request;
                    customer.CustomerName = reader["Name"].ToString();
                }
                else
                {

                    customer = null;
                }

            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
                if (reader != null)
                    reader.Close();
            }

            return customer;
        }
    }
}
