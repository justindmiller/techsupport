﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechSupportData.Model;

namespace TechSupportData.DAL
{
    public class TechniciansDAL
    {

        ///This method will retrive the technicians from the database and populate them into Technician List
        public static List<Technician> GetTechnicians()
        {
            List<Technician> technicianList = new List<Technician>();
            SqlConnection connection = TechSupportDBConnection.GetConnection();
            string selectStatement =
                "SELECT TechID, Name, Email, Phone " +
                "FROM Technicians " +
                "ORDER BY Name";
            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            SqlDataReader reader = null;

            try
            {
                connection.Open();
                reader = selectCommand.ExecuteReader();

                while (reader.Read())
                {
                    Technician technician = new Technician();
                    technician.Name = reader["Name"].ToString();
                    technician.Email = reader["Email"].ToString();
                    technician.TechID = reader["TechID"].ToString();
                    technician.Phone = reader["Phone"].ToString();
                    technicianList.Add(technician);
                }

            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
                if (reader != null)
                    reader.Close();
            }


            return technicianList;
        }

        public static Technician GetTechnician(string request)
        {
            Technician technician = new Technician();
            SqlConnection connection = TechSupportDBConnection.GetConnection();
            string selectStatement =
                "SELECT Name, Email, Phone " +
                "FROM Technicians " +
                "WHERE @TechID = TechID";
            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            selectCommand.Parameters.AddWithValue("@TechID", request);
            SqlDataReader reader = null;
            try
            {
                connection.Open();
                reader = selectCommand.ExecuteReader();

                if (reader.Read())
                {
                    technician.Email = reader["Email"].ToString();
                    technician.TechID = request;
                    technician.Name = reader["Name"].ToString();
                    technician.Phone = reader["Phone"].ToString();
                }
                else
                {
                    technician = null;
                }
                reader.Close();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
                if (reader != null)
                    reader.Close();
            }
            return technician;
        }
    }
}
