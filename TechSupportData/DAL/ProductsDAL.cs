﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechSupportData.Model;

namespace TechSupportData.DAL
{
    /// <summary>
    /// DAL class to retrieve the product list from the database
    /// </summary>
    public class ProductsDAL
    {
        ///This method will retrive the products from the database and populate them into Product List
        public static List<Product> GetProducts()
        {
            List<Product> productList = new List<Product>();
            SqlConnection connection = TechSupportDBConnection.GetConnection();
            string selectStatement =
                "SELECT ProductCode, Name " +
                "FROM Products " +
                "ORDER BY Name";
            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            SqlDataReader reader = null;

            try
            {
                connection.Open();
                reader = selectCommand.ExecuteReader();

                while (reader.Read())
                {
                    Product product = new Product();
                    product.ProductName = reader["Name"].ToString();
                    product.ProductCode = reader["ProductCode"].ToString();
                    productList.Add(product);
                }

            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
                if (reader != null)
                    reader.Close();
            }


            return productList;
        }

        public static Product getProduct(string productCode)
        {
            Product product = new Model.Product();
            SqlConnection connection = TechSupportDBConnection.GetConnection();
            string selectStatement =
                "SELECT Name, Version, ReleaseDate " +
                "FROM Products " +
                "WHERE ProductCode = @ProductCode";
            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            selectCommand.Parameters.AddWithValue("@ProductCode", productCode);
            SqlDataReader reader = null;
            try
            {
                connection.Open();
                reader = selectCommand.ExecuteReader();

                if (reader.Read())
                {
                    product.ProductName = reader["Name"].ToString();
                    product.ProductCode = productCode;
                    product.ReleaseDate = (DateTime)reader["ReleaseDate"];
                    product.Version = reader["Version"].ToString();
                }
                else
                {
                    product = null;
                }
                reader.Close();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
                if (reader != null)
                    reader.Close();
            }
            return product;
        }
    }
}
