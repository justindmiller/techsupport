﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TechSupportData.Model;

namespace TechSupportData.DAL
{
    /// <summary>
    /// This is the DAL class that will display the open incidents
    /// </summary>
    public class IncidentsDAL
    {

        ///This method will retrieve the current open incidents by Technician
        public static List<Incident> GetTechOpenIncidents(string techID)
        {
            List<Incident> incidentList = new List<Incident>();
            SqlConnection connection = TechSupportDBConnection.GetConnection();
            string selectStatement =
                "SELECT i.ProductCode, i.DateOpened, p.Name as Product, c.Name as Customer, t.Name as Technician, i.Title " +
                "FROM Incidents i " +
                "LEFT JOIN Customers c " +
                "ON i.CustomerID = c.CustomerID " +
                "LEFT JOIN Technicians t " +
                "ON t.TechID = i.TechID " +
                "LEFT JOIN Products p " +
                "ON p.ProductCode = i.ProductCode " +
                "WHERE i.DateClosed IS NULL " +
                "AND t.TechID = @techID " +
                "ORDER BY i.DateOpened";
            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            selectCommand.Parameters.AddWithValue("@techID", techID.ToString());
            SqlDataReader reader = null;

            try
            {
                connection.Open();
                reader = selectCommand.ExecuteReader();

                while (reader.Read())
                {
                    Incident incident = new Incident();
                    incident.ProductCode = reader["ProductCode"].ToString();
                    incident.DateOpened = reader["DateOpened"].ToString();
                    incident.Customer = reader["Customer"].ToString();
                    incident.Technician = reader["Technician"].ToString();
                    incident.Title = reader["Title"].ToString();
                    incident.Product = reader["Product"].ToString();
                    incidentList.Add(incident);
                }

            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
                if (reader != null)
                    reader.Close();
            }


            return incidentList;
        }

        ///This method will retrive the open incidents from the database and populate them into Incident List
        public static List<Incident> GetOpenIncidents()
        {
            List<Incident> incidentList = new List<Incident>();
            SqlConnection connection = TechSupportDBConnection.GetConnection();
            string selectStatement =
                "SELECT i.ProductCode, i.DateOpened, c.Name as Customer, t.Name as Technician, i.Title " +
                "FROM Incidents i " +
                "LEFT JOIN Customers c " +
                "ON i.CustomerID = c.CustomerID " +
                "LEFT JOIN Technicians t " +
                "ON t.TechID = i.TechID " +
                "WHERE i.DateClosed IS NULL " +
                "ORDER BY i.DateOpened";
            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            SqlDataReader reader = null;

            try
            {
                connection.Open();
                reader = selectCommand.ExecuteReader();

                while (reader.Read())
                {
                    Incident incident = new Incident();
                    incident.ProductCode = reader["ProductCode"].ToString();
                    incident.DateOpened = reader["DateOpened"].ToString();
                    incident.Customer = reader["Customer"].ToString();
                    incident.Technician = reader["Technician"].ToString();
                    incident.Title = reader["Title"].ToString();
                    incidentList.Add(incident);
                }

            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
                if (reader != null)
                    reader.Close();
            }


            return incidentList;
        }

        public static Incident GetIncident(int incidentID)
        {
            Incident incident = new Incident();
            SqlConnection connection = TechSupportDBConnection.GetConnection();
            string selectStatement = "SELECT IncidentID, ProductCode, DateOpened, DateClosed, CustomerID, TechID, Title, Description " +
                                     "FROM Incidents " +
                                     "WHERE @IncidentID = IncidentID";
            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            selectCommand.Parameters.AddWithValue("@IncidentID", incidentID.ToString());
            
            try
            {
                connection.Open();
                SqlDataReader reader =
                    selectCommand.ExecuteReader(CommandBehavior.SingleRow);
                if (reader.Read())
                {
                    incident.IncidentID = reader["IncidentID"].ToString();
                    incident.ProductCode = reader["ProductCode"].ToString();
                    incident.DateOpened = reader["DateOpened"].ToString();
                    incident.DateClosed = reader["DateClosed"].ToString();
                    incident.CustomerID = reader["CustomerID"].ToString();
                    incident.TechID = reader["TechID"].ToString();
                    incident.Title = reader["Title"].ToString();
                    incident.Description = reader["Description"].ToString();
                }
                else
                {

                    incident = null;
                }

            }
            catch (SqlException ex)
            {

                throw ex;
            }
            catch (Exception)
            {
                MessageBox.Show("Hey!");
            }
            finally
            {
                connection.Close();
            }

            return incident;
        }

        public static void createIncident(Incident incident)
        {
            SqlConnection connection = TechSupportDBConnection.GetConnection();
            string insertStatement = "Insert Incidents " +
                 "(CustomerID, ProductCode, DateOpened, Title, Description) " +
                 "VALUES (@CustomerID, @ProductCode, @DateOpened, @Title, @Description)";
            SqlCommand insertCommand = new SqlCommand(insertStatement, connection);
            insertCommand.Parameters.AddWithValue("@CustomerID", incident.CustomerID);
            insertCommand.Parameters.AddWithValue("@ProductCode", incident.ProductCode);
            insertCommand.Parameters.AddWithValue("@DateOpened", incident.DateOpened);
            insertCommand.Parameters.AddWithValue("@Title", incident.Title);
            insertCommand.Parameters.AddWithValue("@Description", incident.Description);
            try
            {
                connection.Open();
                insertCommand.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }

        public static bool updateIncident(Incident oldIncident, Incident newIncident)
        {
            SqlConnection connection = TechSupportDBConnection.GetConnection();
            string updateStatement =
                "UPDATE Incidents SET " +
                    "CustomerID = @CustomerID, " +
                    "ProductCode = @ProductCode, " +
                    "TechID = @TechID, " +
                    "Title = @Title, " +
                    "DateOpened = @DateOpened, " +
                    "DateClosed = @DateClosed, " +
                    "Description = @Description " +
                "WHERE IncidentID = @OldIncidentID " +
                "AND DateClosed IS NULL";
            SqlCommand updateCommand = new SqlCommand(updateStatement, connection);
            updateCommand.Parameters.AddWithValue("@CustomerID", newIncident.CustomerID);
            updateCommand.Parameters.AddWithValue("@ProductCode", newIncident.ProductCode);
            updateCommand.Parameters.AddWithValue("@TechID", newIncident.TechID);
            updateCommand.Parameters.AddWithValue("@Title", newIncident.Title);
            updateCommand.Parameters.AddWithValue("@DateOpened", newIncident.DateOpened);
            updateCommand.Parameters.AddWithValue("@DateClosed", DBNull.Value);
            updateCommand.Parameters.AddWithValue("@Description", newIncident.Description);

            updateCommand.Parameters.AddWithValue("@OldIncidentID", oldIncident.IncidentID);
            try
            {
                connection.Open();
                int count = updateCommand.ExecuteNonQuery();
                if (count > 0)
                    return true;
                else
                    return false;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }

        public static bool CloseIncident(Incident oldIncident, Incident newIncident)
        {
            SqlConnection connection = TechSupportDBConnection.GetConnection();
            string updateStatement =
                "UPDATE Incidents SET " +
                    "CustomerID = @CustomerID, " +
                    "ProductCode = @ProductCode, " +
                    "TechID = @TechID, " +
                    "Title = @Title, " +
                    "DateOpened = @DateOpened, " +
                    "DateClosed = @DateClosed, " +
                    "Description = @Description " +
                "WHERE IncidentID = @OldIncidentID " +
                "AND DateClosed IS NULL"; ;
            SqlCommand updateCommand = new SqlCommand(updateStatement, connection);
            updateCommand.Parameters.AddWithValue("@CustomerID", newIncident.CustomerID);
            updateCommand.Parameters.AddWithValue("@ProductCode", newIncident.ProductCode);
            updateCommand.Parameters.AddWithValue("@TechID", newIncident.TechID);
            updateCommand.Parameters.AddWithValue("@Title", newIncident.Title);
            updateCommand.Parameters.AddWithValue("@DateOpened", newIncident.DateOpened);
            updateCommand.Parameters.AddWithValue("@DateClosed", DateTime.Now);
            updateCommand.Parameters.AddWithValue("@Description", newIncident.Description);

            updateCommand.Parameters.AddWithValue("@OldIncidentID", oldIncident.IncidentID);
            try
            {
                connection.Open();
                int count = updateCommand.ExecuteNonQuery();
                if (count > 0)
                    return true;
                else
                    return false;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
    }
}
