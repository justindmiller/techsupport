﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TechSupport.Controller;
using TechSupportData.Model;

namespace TechSupport
{
    public partial class ViewIncidentByTechnician : Form
    {
        public ViewIncidentByTechnician()
        {
            InitializeComponent();
        }

        List<Technician> techList;
        private void ViewIncidentByTechnician_Load(object sender, EventArgs e)
        {
            try
            {
                techList = TechSupportController.GetTechnicians();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
                this.BeginInvoke(new MethodInvoker(Close));
            }

            nameComboBox.DataSource = techList;
        }

        List<Incident> incidentList;
        private void nameComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (nameComboBox.SelectedIndex < 0)
            {
                return;
            }
            Technician tech = techList[nameComboBox.SelectedIndex];
            technicianBindingSource.Clear();
            technicianBindingSource.Add(tech);
            string techID = nameComboBox.SelectedValue.ToString();
            try
            {
                incidentList = TechSupportController.GetTechOpenIncidents(techID);
                incidentDataGridView.DataSource = incidentList;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

    }
}
