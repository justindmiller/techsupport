﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechSupportData.DAL;
using TechSupportData.Model;

namespace TechSupport.Controller
{
    /// <summary>
    /// Controller class that deals with the DAL to prevent coupling with the View and DAL
    /// </summary>
    public class TechSupportController
    {
        public static List<Incident> GetOpenIncidents()
        {
            return IncidentsDAL.GetOpenIncidents();
        }

        public static List<Customer> GetCustomers()
        {
            return CustomersDAL.GetCustomers();
        }

        public static List<Product> GetProducts()
        {
            return ProductsDAL.GetProducts();
        }

        public static void CreateIncident(Incident incident)
        {
            IncidentsDAL.createIncident(incident);
        }

        public static List<Technician> GetTechnicians()
        {
            return TechniciansDAL.GetTechnicians();
        }

        public static List<Incident> GetTechOpenIncidents(string techID)
        {
            return IncidentsDAL.GetTechOpenIncidents(techID);
        }

        public static bool UpdateIncident(Incident oldIncident, Incident newIncident)
        {
            return IncidentsDAL.updateIncident(oldIncident, newIncident);
        }

        public static bool CloseIncident(Incident oldIncident, Incident newIncident)
        {
            return IncidentsDAL.CloseIncident(oldIncident, newIncident);
        }

        public static Incident GetIncident(int IncidentID)
        {
            return IncidentsDAL.GetIncident(IncidentID);
        }

        public static Product GetProduct(string ProductID)
        {
            return ProductsDAL.getProduct(ProductID);
        }

        public static Customer GetCustomer(string CustomerID)
        {
            return CustomersDAL.getCustomer(CustomerID);
        }

        public static Technician GetTechnician(string TechID)
        {
            return TechniciansDAL.GetTechnician(TechID);
        }



    }
}
