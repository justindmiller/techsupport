﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TechSupport.View
{
    public partial class TechSupportGUI : Form
    {
        public TechSupportGUI()
        {
            InitializeComponent();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        DisplayOpenIncidents incidentDisplay;
        private void displayOpenIncidentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (incidentDisplay == null)
            {
                incidentDisplay = new View.DisplayOpenIncidents();
                incidentDisplay.MdiParent = this;
                incidentDisplay.FormClosed += new FormClosedEventHandler(incidentDisplay_FormClosed);
                incidentDisplay.Show();
            }
            else
                incidentDisplay.Activate();
        }

        private void incidentDisplay_FormClosed(object sender, FormClosedEventArgs e)
        {
            incidentDisplay = null;
        }

        CreateIncident incidentCreation;
        private void createIncidentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (incidentCreation == null)
            {
                incidentCreation = new View.CreateIncident();
                incidentCreation.MdiParent = this;
                incidentCreation.FormClosed += new FormClosedEventHandler(createIncident_FormClosed);
                incidentCreation.Show();
            }
            else
                incidentCreation.Activate();
        }

        private void createIncident_FormClosed(object sender, FormClosedEventArgs e)
        {
            incidentCreation = null;
        }

        UpdateIncident incidentUpdate;
        private void updateIncidentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (incidentUpdate == null)
            {
            incidentUpdate = new View.UpdateIncident();
            incidentUpdate.MdiParent = this;
            incidentUpdate.FormClosed += new FormClosedEventHandler(updateIncident_FormClosed);
            incidentUpdate.Show();
            }
            else
                incidentUpdate.Activate();
        }

        private void updateIncident_FormClosed(object sender, FormClosedEventArgs e)
        {
            incidentUpdate = null;
        }

        ViewIncidentByTechnician byTechnician;
        private void viewOpenIncidentByTechnicianToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (byTechnician == null)
            {
                byTechnician = new ViewIncidentByTechnician();
                byTechnician.MdiParent = this;
                byTechnician.FormClosed += new FormClosedEventHandler(byTechician_FormClosed);
                byTechnician.Show();
            }
            else
                byTechnician.Activate();
        }

        private void byTechician_FormClosed(object sender, FormClosedEventArgs e)
        {
            byTechnician = null;
        }

        DisplayReports displayReport;
        private void displayIncidentsByProductsAndTechnicianToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (displayReport  == null)
            {
                displayReport = new DisplayReports();
                displayReport.MdiParent = this;
                displayReport.FormClosed += new FormClosedEventHandler(displayReport_FormClosed);
                displayReport.Show();
            }
            else
                displayReport.Activate();
        }

        private void displayReport_FormClosed(object sender, FormClosedEventArgs e)
        {
            displayReport = null;
        }
    }
}
