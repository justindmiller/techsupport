﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TechSupport.View
{
    public partial class DisplayReports : Form
    {
        public DisplayReports()
        {
            InitializeComponent();
        }

        private void DisplayReports_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'TechReports.Incidents' table. You can move, or remove it, as needed.
            this.IncidentsTableAdapter.Fill(this.TechReports.Incidents);

            this.reportViewer1.RefreshReport();
        }
    }
}
