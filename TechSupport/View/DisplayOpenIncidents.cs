﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TechSupport.Controller;
using TechSupportData.Model;

namespace TechSupport.View
{
    /// <summary>
    /// Displays the current Open Incidents for view
    /// </summary>
    public partial class DisplayOpenIncidents : Form
    {
        private TechSupportController incidentController;
        private List<Incident> incidentList;
        public DisplayOpenIncidents()
        {
            InitializeComponent();
            incidentController = new TechSupportController();
        }

        private void DisplayOpenIncidents_Load(object sender, EventArgs e)
        {
            
            try
            {
                this.incidentList = TechSupportController.GetOpenIncidents();
                if (this.incidentList.Count > 0)
                {
                    Incident incident;
                    for (int i = 0; i < incidentList.Count; i++)
                    {
                        incident = this.incidentList[i];
                        lvIncidents.Items.Add(incident.ProductCode.ToString());
                        lvIncidents.Items[i].SubItems.Add(incident.DateOpened.ToString());
                        lvIncidents.Items[i].SubItems.Add(incident.Customer.ToString());
                        lvIncidents.Items[i].SubItems.Add(incident.Technician.ToString());
                        lvIncidents.Items[i].SubItems.Add(incident.Title.ToString());
                    }
                }
                else
                {
                    MessageBox.Show("No open incidents.", "All incidents resolved at present.");
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
                this.BeginInvoke(new MethodInvoker(Close));
            }

        }
    }
}
