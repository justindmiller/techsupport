﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TechSupport.Controller;
using TechSupportData.Model;

namespace TechSupport.View
{



    public partial class UpdateIncident : Form
    {



        public UpdateIncident()
        {
            InitializeComponent();
        }

        Incident incident;

        private void getButton_Click(object sender, EventArgs e)
        {
            DescriptionBox.ReadOnly = true;
            UpdateButton.Enabled = false;
            CloseButton.Enabled = false;
            TextAddBox.Enabled = false;
            TechCombo.Enabled = false;
            if (Validator.IsPresent(txtIncidentID) &&
               Validator.IsInt32(txtIncidentID))
            {
                int incidentID = Convert.ToInt32(txtIncidentID.Text);
                this.clearValues();
                this.GetIncident(incidentID);
            }

        }

        private void GetIncident(int incidentID)
        {

            try
            {
                incident = TechSupportController.GetIncident(incidentID);
                incident.Customer = TechSupportController.GetCustomer(incident.CustomerID).CustomerName;
                if (incident.DateClosed == "")
                {
                    UpdateButton.Enabled = true;
                    CloseButton.Enabled = true;
                    TextAddBox.Enabled = true;
                    TechCombo.Enabled = true;
                }
                this.DisplayIncident();
                TextAddBox.Clear();
            }
            catch (Exception)
            {
                MessageBox.Show("No incident found with this ID. " +
                   "Please try again.", "Incident Not Found");
            }
        }

        private void DisplayIncident()
        {
            this.LoadComboBoxes();
            CustomerBox.Text = incident.Customer;
            ProductBox.Text = incident.ProductCode;
            TechCombo.SelectedValue = incident.TechID;
            TitleBox.Text = incident.Title;
            DateOpened.Text = incident.DateOpened;
            DescriptionBox.Text = incident.Description;
        }


        List<Technician> techList;
        private void LoadComboBoxes()
        {
            try
            {
                techList = TechSupportController.GetTechnicians();
                TechCombo.DataSource = techList;
                TechCombo.DisplayMember = "Name";
                TechCombo.ValueMember = "TechID";

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
                this.BeginInvoke(new MethodInvoker(Close));
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void UpdateIncident_Load(object sender, EventArgs e)
        {
            UpdateButton.Enabled = false;
            CloseButton.Enabled = false;
            TextAddBox.Enabled = false;
            TechCombo.Enabled = false;
        }

        private void clearValues()
        {
            incident = new Incident();
            incident.CustomerID = "";
            incident.ProductCode = "";
            incident.TechID = "";
            incident.Title = "";
            incident.DateOpened = "";
            incident.DateClosed = "";
            incident.Description = "";
        }

        private Incident setUpdatedIncident()
        {
            Incident setUpdatedIncident = new Incident();
            setUpdatedIncident.CustomerID = incident.CustomerID;
            setUpdatedIncident.ProductCode = ProductBox.Text;
            setUpdatedIncident.TechID = TechCombo.SelectedValue.ToString();
            setUpdatedIncident.Title = TitleBox.Text;
            setUpdatedIncident.DateOpened = DateOpened.Text;
            if (incident.Description.Length >= 200)
            {

            }
            if (TextAddBox.Text == "" && (incident.Description.Length + "\n".Length + DateTime.Today.ToShortDateString().Length + " ".Length + TextAddBox.Text.Length) <= 200)
            {
                setUpdatedIncident.Description = DescriptionBox.Text + "\n" + DateTime.Today.ToShortDateString() + " updated/assigned the technician";
            }
            else if ((incident.Description.Length + "\n".Length + DateTime.Today.ToShortDateString().Length + " ".Length + TextAddBox.Text.Length) <= 200)
                setUpdatedIncident.Description = DescriptionBox.Text + "\n" + DateTime.Today.ToShortDateString() + " " + TextAddBox.Text;
            else
                setUpdatedIncident.Description = incident.Description;
            return setUpdatedIncident;
        }

        private void UpdateButton_Click(object sender, EventArgs e)
        {
            if (TechCombo.Text == "")
            {
                MessageBox.Show("Technician must be assigned!");
                return;
            }
            if (IsValidLength())
            {
                this.attemptUpdate();
                DescriptionBox.ReadOnly = true;
            }
            else
            {
                DialogResult result = MessageBox.Show("Truncate " + incident.IncidentID + "?",
                    "Confirm Truncate", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    this.truncateDescription(TextAddBox.Text);
                    this.attemptUpdate();
                }
                else if (result == DialogResult.No)
                {
                    MessageBox.Show("Please edit the description below 200 characters.");
                    DescriptionBox.ReadOnly = false;
                    TextAddBox.Text = "Description Edits (erase this for more space)";
                }
                else
                {
                    MessageBox.Show("Update canceled!");
                    return;
                }
            }
            TextAddBox.Clear();
        }

        private bool IsValidLength()
        {
            if ((incident.Description.Length + "\n".Length + DateTime.Today.ToShortDateString().Length + TextAddBox.Text.Length) <= 200)
            {
                return true;
            } else
            {
                return false;
            }
        }

        private void attemptUpdate()
        {
            try
            {
                if (!(TechSupportController.UpdateIncident(incident, this.setUpdatedIncident())))
                {
                    MessageBox.Show("Another user has updated or deleted " +
                        "that incident.", "Database Error");
                    this.clearValues();
                    this.GetIncident(Convert.ToInt32(txtIncidentID.Text));
                }
                else
                {
                    this.clearValues();
                    this.GetIncident(Convert.ToInt32(txtIncidentID.Text));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
                this.BeginInvoke(new MethodInvoker(Close));
            }
        }

        private void truncateDescription(string text)
        {
            incident.Description = string.Concat(text + incident.Description.Take(200));
            this.Refresh();
        }

        private void DescriptionBox_TextChanged(object sender, EventArgs e)
        {
            if (DescriptionBox.Text.Length == 200) {
                MessageBox.Show("Max characters of 200 reached!");
                TextAddBox.Enabled = false;
            }
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            if (TechCombo.Text == "")
            {
                MessageBox.Show("Technician must be assigned!");
                return;
            }
            DialogResult result = MessageBox.Show("Close " + incident.IncidentID + "? Incident cannot be updated after closed!",
                  "Confirm Incident Close!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                this.attemptClose();
                this.clearValues();
                this.GetIncident(Convert.ToInt32(txtIncidentID.Text));
            }
            else
            {
                return;
            }
        }

        private void attemptClose()
        {
            try
            {
                if (!(TechSupportController.CloseIncident(incident, this.setUpdatedIncident())))
                {
                    MessageBox.Show("Another user has updated or deleted " +
                        "that incident.", "Database Error");
                    this.clearValues();
                    this.GetIncident(Convert.ToInt32(txtIncidentID.Text));
                }
                else
                {
                    DescriptionBox.ReadOnly = true;
                    UpdateButton.Enabled = false;
                    CloseButton.Enabled = false;
                    TextAddBox.Enabled = false;
                    TechCombo.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
                this.BeginInvoke(new MethodInvoker(Close));
            }
        }

    }
}
