﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TechSupport.Controller;
using TechSupportData.Model;

namespace TechSupport.View
{
    public partial class CreateIncident : Form
    {
        public CreateIncident()
        {
            InitializeComponent();
        }

        private List<Customer> customerList;
        private List<Product> productList;

        private void btnCreate_Click(object sender, EventArgs e)
        {
            Incident newIncident = new Incident();
            newIncident.CustomerID = cboCustomer.SelectedValue.ToString();
            newIncident.ProductCode = cboProduct.SelectedValue.ToString();
            newIncident.Title = textTitle.Text;
            newIncident.DateOpened = System.DateTime.Now.ToString();
            newIncident.Description = textDescription.Text;
            if (newIncident.Title == "")
            {
                MessageBox.Show("Title cannot be empty", "Input Error");
            }
            else if (newIncident.Description == "")
            {
                MessageBox.Show("Description cannot be empty", "Input Error");
            }
            else
            {
                TechSupportController.CreateIncident(newIncident);
                MessageBox.Show("Incident " + newIncident.Title + " added!", "Incident Accepted");
                this.Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LoadComboBoxes()
        {
            try
            {
                this.customerList = TechSupportController.GetCustomers();
                cboCustomer.DataSource = customerList;
                cboCustomer.DisplayMember = "CustomerName";
                cboCustomer.ValueMember = "CustomerID";

                productList = TechSupportController.GetProducts();
                cboProduct.DataSource = productList;
                cboProduct.DisplayMember = "ProductName";
                cboProduct.ValueMember = "ProductCode";

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
                this.BeginInvoke(new MethodInvoker(Close));
            }
        }

        private void CreateIncident_Load(object sender, EventArgs e)
        {
            this.LoadComboBoxes();
        }
    }
}
