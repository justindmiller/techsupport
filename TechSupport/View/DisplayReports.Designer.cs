﻿namespace TechSupport.View
{
    partial class DisplayReports
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.TechReports = new TechSupport.View.TechReports();
            this.IncidentsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.IncidentsTableAdapter = new TechSupport.View.TechReportsTableAdapters.IncidentsTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.TechReports)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IncidentsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // reportViewer1
            // 
            this.reportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "OpenTech";
            reportDataSource1.Value = this.IncidentsBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "TechSupport.View.TechReport.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(0, 0);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.Size = new System.Drawing.Size(754, 400);
            this.reportViewer1.TabIndex = 0;
            // 
            // TechReports
            // 
            this.TechReports.DataSetName = "TechReports";
            this.TechReports.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // IncidentsBindingSource
            // 
            this.IncidentsBindingSource.DataMember = "Incidents";
            this.IncidentsBindingSource.DataSource = this.TechReports;
            // 
            // IncidentsTableAdapter
            // 
            this.IncidentsTableAdapter.ClearBeforeFill = true;
            // 
            // DisplayReports
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(754, 400);
            this.Controls.Add(this.reportViewer1);
            this.Name = "DisplayReports";
            this.Text = "DisplayReports";
            this.Load += new System.EventHandler(this.DisplayReports_Load);
            ((System.ComponentModel.ISupportInitialize)(this.TechReports)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IncidentsBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.BindingSource IncidentsBindingSource;
        private TechReports TechReports;
        private TechReportsTableAdapters.IncidentsTableAdapter IncidentsTableAdapter;
    }
}