﻿namespace TechSupport.View
{
    partial class UpdateIncident
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtIncidentID = new System.Windows.Forms.TextBox();
            this.DateOpened = new System.Windows.Forms.TextBox();
            this.DescriptionBox = new System.Windows.Forms.RichTextBox();
            this.TextAddBox = new System.Windows.Forms.RichTextBox();
            this.getButton = new System.Windows.Forms.Button();
            this.UpdateButton = new System.Windows.Forms.Button();
            this.CloseButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.CustomerBox = new System.Windows.Forms.TextBox();
            this.TechCombo = new System.Windows.Forms.ComboBox();
            this.ProductBox = new System.Windows.Forms.TextBox();
            this.TitleBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Incident ID:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Customer:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 112);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Product:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(23, 162);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Technician";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(23, 209);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(27, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Title";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(23, 250);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Date Opened:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(23, 298);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(63, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Description:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(23, 412);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(69, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Text To Add:";
            // 
            // txtIncidentID
            // 
            this.txtIncidentID.Location = new System.Drawing.Point(105, 19);
            this.txtIncidentID.Name = "txtIncidentID";
            this.txtIncidentID.Size = new System.Drawing.Size(100, 20);
            this.txtIncidentID.TabIndex = 8;
            // 
            // DateOpened
            // 
            this.DateOpened.Location = new System.Drawing.Point(105, 250);
            this.DateOpened.Name = "DateOpened";
            this.DateOpened.ReadOnly = true;
            this.DateOpened.Size = new System.Drawing.Size(150, 20);
            this.DateOpened.TabIndex = 9;
            // 
            // DescriptionBox
            // 
            this.DescriptionBox.Location = new System.Drawing.Point(105, 295);
            this.DescriptionBox.MaxLength = 200;
            this.DescriptionBox.Name = "DescriptionBox";
            this.DescriptionBox.ReadOnly = true;
            this.DescriptionBox.Size = new System.Drawing.Size(274, 111);
            this.DescriptionBox.TabIndex = 10;
            this.DescriptionBox.Text = "";
            this.DescriptionBox.TextChanged += new System.EventHandler(this.DescriptionBox_TextChanged);
            // 
            // TextAddBox
            // 
            this.TextAddBox.Location = new System.Drawing.Point(105, 412);
            this.TextAddBox.MaxLength = 0;
            this.TextAddBox.Name = "TextAddBox";
            this.TextAddBox.Size = new System.Drawing.Size(274, 96);
            this.TextAddBox.TabIndex = 11;
            this.TextAddBox.Text = "";
            // 
            // getButton
            // 
            this.getButton.Location = new System.Drawing.Point(234, 17);
            this.getButton.Name = "getButton";
            this.getButton.Size = new System.Drawing.Size(75, 23);
            this.getButton.TabIndex = 12;
            this.getButton.Text = "Get";
            this.getButton.UseVisualStyleBackColor = true;
            this.getButton.Click += new System.EventHandler(this.getButton_Click);
            // 
            // UpdateButton
            // 
            this.UpdateButton.Location = new System.Drawing.Point(105, 514);
            this.UpdateButton.Name = "UpdateButton";
            this.UpdateButton.Size = new System.Drawing.Size(76, 25);
            this.UpdateButton.TabIndex = 13;
            this.UpdateButton.Text = "Update";
            this.UpdateButton.UseVisualStyleBackColor = true;
            this.UpdateButton.Click += new System.EventHandler(this.UpdateButton_Click);
            // 
            // CloseButton
            // 
            this.CloseButton.Location = new System.Drawing.Point(207, 514);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(75, 23);
            this.CloseButton.TabIndex = 14;
            this.CloseButton.Text = "Close";
            this.CloseButton.UseVisualStyleBackColor = true;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(304, 514);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 15;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // CustomerBox
            // 
            this.CustomerBox.Location = new System.Drawing.Point(105, 64);
            this.CustomerBox.Name = "CustomerBox";
            this.CustomerBox.ReadOnly = true;
            this.CustomerBox.Size = new System.Drawing.Size(274, 20);
            this.CustomerBox.TabIndex = 16;
            // 
            // TechCombo
            // 
            this.TechCombo.FormattingEnabled = true;
            this.TechCombo.Location = new System.Drawing.Point(105, 159);
            this.TechCombo.Name = "TechCombo";
            this.TechCombo.Size = new System.Drawing.Size(274, 21);
            this.TechCombo.TabIndex = 18;
            // 
            // ProductBox
            // 
            this.ProductBox.Location = new System.Drawing.Point(105, 112);
            this.ProductBox.Name = "ProductBox";
            this.ProductBox.ReadOnly = true;
            this.ProductBox.Size = new System.Drawing.Size(274, 20);
            this.ProductBox.TabIndex = 19;
            // 
            // TitleBox
            // 
            this.TitleBox.Location = new System.Drawing.Point(105, 206);
            this.TitleBox.Name = "TitleBox";
            this.TitleBox.ReadOnly = true;
            this.TitleBox.Size = new System.Drawing.Size(274, 20);
            this.TitleBox.TabIndex = 20;
            // 
            // UpdateIncident
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(444, 568);
            this.Controls.Add(this.TitleBox);
            this.Controls.Add(this.ProductBox);
            this.Controls.Add(this.TechCombo);
            this.Controls.Add(this.CustomerBox);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.UpdateButton);
            this.Controls.Add(this.getButton);
            this.Controls.Add(this.TextAddBox);
            this.Controls.Add(this.DescriptionBox);
            this.Controls.Add(this.DateOpened);
            this.Controls.Add(this.txtIncidentID);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "UpdateIncident";
            this.Text = "Update Incident";
            this.Load += new System.EventHandler(this.UpdateIncident_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtIncidentID;
        private System.Windows.Forms.TextBox DateOpened;
        private System.Windows.Forms.RichTextBox DescriptionBox;
        private System.Windows.Forms.RichTextBox TextAddBox;
        private System.Windows.Forms.Button getButton;
        private System.Windows.Forms.Button UpdateButton;
        private System.Windows.Forms.Button CloseButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.TextBox CustomerBox;
        private System.Windows.Forms.ComboBox TechCombo;
        private System.Windows.Forms.TextBox ProductBox;
        private System.Windows.Forms.TextBox TitleBox;
    }
}